# walc-vars collects variable definitions for usage in other scripts

## Script Description
This is a bash script which "walks" throught different folders to parse ini-files
(without [sections]). Itself also provide the collected variables in ini-style
(varname=value). There is a focus on Web Applications but the script can also be
used other things.

## Dependencies
Because of a associative arrays we need bash >= 4. To locate bash on alternate paths /usr/bin/env is be used.

## Rules to change variable names and values
* Lines starting with comment marks "#" and ";" are ignored.
* Empty definitions (varname= ) are ignored.
* All variable names are transformed to lowercase.
* From values quotation marks ("|') and trailing slashes are removed.

## Special "walc" prefix and it's functionality
For special support of Web Applications (especially Drupal) you can use some
variables with a "walc_"-prefix:
* walc_code (path to the code, will be used to load ini files from web application)
* walc_domain (domain of the Web Application)
* walc_host (hostname to connect)
* walc_db (database name)
* walc_dbuser (database username, if not defined it's set to walc_user)
* walc_env (define an environment, will be used to load environment ini files)
* walc_user (will be set via "whoami" to current user if not defined)

## Ini Files and Folders
There are three types of ini-files with corresponding prefix with "_" like "base_":
* base (main config context for walc iteself, web applicaton, and local server)
* pull (server config e.g. to pull data from content provider on dev systems)
* push (server config e.g. to push data to backup server on production systems)

There are five variations of ini files in order to load/override (* > base|pull|push):
1. *.ini (e.g. to be organized by server configuration scripts or code repository)
2. custom-filename.ini (only ~/walc/, define with --home-*)
3. *-local.ini (e.g. to switch web application enviroment context)
4. custom-local-filename.ini (only ~/walc/, define with --home-local-*)
5. *-env-name.ini (when walc_env=name is defined before in base context)
6. *-override.ini (override variables for testing or security reasons)

Finally there are three locations to place ini files:
* /etc/walc/
* ~/walc/
* [base_walc_code]/walc/ (when var "walc_code" is defined before in base context or flag "--pwd-code" is used to define current folder as base_walc_code)

This is the order to load most of the ini files. Only the override ini files are
loaded in a backward order. So first one on code folder and the last one in /etc/.
On this way it's possible to define special variables which cannot be overidden by
normal users e.g. developers.

## About the project.

If you want to organize Web Applications with commandline for database backups and
special commandline tools you always need some information of the environment and
the current configuration of an Application.
In Drupal context we usually store commandline in the code folder since we use
composer to manage the code. But composer is very flexible and there are different
strategies to place binaries.
Some projects use default folder of composer which is [base_walc_code]/vendor/bin
and other projects uses [base_walc_code]/bin.
In a context where you have all this different projects to maintain with many
support users to step in quick if needed it's helpful to create some standards.
To solve this situation you can now place a new folder "walc" in your code project
and a script path relative: "foo=vendor/bin/bar.sh" or two real world examples:
"drush=vendor/bin/drush" (defined in "[base_walc_code]/walc/base.ini"). Or you need
a full path e.g. for a backup target folder: "bar=/home/username/upload-folder"
(defined  in "~/walc/push.ini"). With walc-vars you can now use self this defined
variables as "$base_foo", "$push_bar", and "$base_drush" in your own scripts.

## The history and the name
As a web developer I was on a way to json but our hosting partner pointed on the
more simple old ini solution. Because I see a need of walc variables in different
scripts I decided to create an own project. So walc-vars can now be used by others
and our own specialised scripts for backups, dayly maintenance and deployment
tasks.

But there was a need for a name also for additional scripts so I ended in
"WALC: Web Application Linux Commands". This is not very descriptive but short and
meaningful. This "main" script walc-vars can be seen as script which is walking
through the ini files. This is good to remember the command.
But before I decided to name this "walc" I searched the WWW for more meanings.
I only found that there is the polish word "walc" which means "waltz".
So I hope my script can also be seen as it is dancing through the ini files :-)

---
* Author: Carsten Logemann ([Nodegard GmbH](https://nodegard.com/))
* Project home: https://gitlab.com/walc/walc-vars
